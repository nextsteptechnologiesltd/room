package pin

import (
	"fmt"
	"os"
	"time"

	rpio "github.com/stianeikeland/go-rpio"
)

var (
	pin = rpio.Pin(21)

	// Occupied represents whether or not motion has been detected recently
	Occupied = false
)

func checkPin() bool {
	if err := rpio.Open(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	defer rpio.Close()

	pin.Input()
	res := pin.Read()

	return res == rpio.High
}

// Monitor will continuously check the state of the motion sensor
func Monitor() {
	fmt.Println("Monitoring occupation of room...")
	var readings []bool
	for {
		reading := checkPin()
		fmt.Println("Occupied: ", reading)
		readings = append(readings, reading)
		if len(readings) > 20 {
			fmt.Println("Checking readings")
			detectedMotion := 0
			for _, r := range readings {
				if r {
					detectedMotion++
				}
			}
			fmt.Println("Number of motion detections: ", detectedMotion)
			Occupied = detectedMotion >= 5
			fmt.Println("Occupied status: ", Occupied)
			readings = readings[:0]
		}
		time.Sleep(time.Second)
	}
}
